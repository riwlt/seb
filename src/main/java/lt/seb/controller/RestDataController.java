package lt.seb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import lt.seb.model.Customer;
import lt.seb.model.Product;
import lt.seb.model.Rules;
import lt.seb.verifier.VerifyProduct;

@RestController
public class RestDataController {

	@RequestMapping("/api/add/customer")
	public List<Product> returnProduct(@RequestParam(value = "customerData", required = true) String customerData)
			throws IOException {
		ObjectMapper mapper = new ObjectMapper(); 
		Customer cust = mapper.readValue(customerData, Customer.class);
		return this.filterProductByCustomer(cust);
	}
 
	public List<Product> filterProductByCustomer(Customer cust) {
		List<Product> productList = this.addProducts();
		List<Product> correctList = new ArrayList<>();
		
		for (Product p : productList) {
			for (Rules r : p.getProductRules()) {
				VerifyProduct v = new VerifyProduct();
				if(v.verifyProductByCustomer(cust, r)) correctList.add(p);
			}
		}
	
		return correctList; 
	}

	public static List<Rules> addRule(int minAge, int maxAge, int minIncome, int maxIncome, boolean studentRequirement) {
		List<Rules> ruleList = new ArrayList<>();
		Rules rule = new Rules();
		rule.setMaxAge(maxAge);
		rule.setMaxIncome(maxIncome);
		rule.setMinAge(minAge);
		rule.setMinIncome(minIncome);
		rule.setStudentRequirement(studentRequirement);
		ruleList.add(rule);
		return ruleList;
	}

	public List<Product> addProducts(){
		List<Product> productList = new ArrayList<>();
		Product product1 = new Product("Current Account", RestDataController.addRule(17, 150, 0, 2147483647, false));
		Product product2 = new Product("Current Account Plus", RestDataController.addRule(17, 150, 40000, 2147483647, false));
		Product product3 = new Product("Junior Saver Account", RestDataController.addRule(0, 18, 0, 2147483647, false));
		Product product4 = new Product("Student Account", RestDataController.addRule(17, 150, 0, 2147483647, true));
		Product product5 = new Product("Senior Account", RestDataController.addRule(65, 150, 0, 2147483647, false));
		Product product6 = new Product("Debit Card", RestDataController.addRule(17, 150, 0, 12000, false));
		Product product7 = new Product("Credit Card", RestDataController.addRule(17, 150, 12000, 2147483647, false));
		Product product8 = new Product("Gold Credit Card", RestDataController.addRule(17, 150, 40000, 2147483647, false));
		Collections.addAll(productList, product1, product2, product3, product4, product5, product6, product7, product8);
		return productList;
	}
	
}
