package lt.seb.verifier;

import lt.seb.model.Customer;
import lt.seb.model.Rules;

public class VerifyProduct {

	public boolean verifyProductByCustomer(Customer cust, Rules r) {
		return isAgeCorrect(cust, r) && isIncomeCorrect(cust, r) && isCustomerStudent(cust, r);
	}

	private static boolean isAgeCorrect(Customer cust, Rules r) {
		return cust.getAge() >= r.getMinAge() && cust.getAge() < r.getMaxAge();
	}

	private static boolean isIncomeCorrect(Customer cust, Rules r) {
		return cust.getIncome() >= r.getMinIncome() && cust.getIncome() <= r.getMaxIncome();
	}

	private static boolean isCustomerStudent(Customer cust, Rules r) {
		return cust.isStudent() == r.isStudentRequirement();
	}
}
