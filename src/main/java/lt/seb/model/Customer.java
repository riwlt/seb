package lt.seb.model;

public class Customer {

	private int age;
	private int income;
	private boolean isStudent;

	public Customer(int age, int income, boolean isStudent) {
		super();
		this.age = age;
		this.income = income;
		this.isStudent = isStudent;
	}
	
	public Customer() {
		super();
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getIncome() {
		return income;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	public boolean isStudent() {
		return isStudent;
	}

	public void setStudent(boolean isStudent) {
		this.isStudent = isStudent;
	}

}
