package lt.seb.model;

import java.util.List;

public class Product {

	private String productName;
	private List<Rules> productRules;
	
	public Product(String productName, List<Rules> productRules) {
		super();
		this.productName = productName;
		this.productRules = productRules;
	}

	public Product() {
		super();
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Rules> getProductRules() {
		return productRules;
	}

	public void setProductRules(List<Rules> productRules) {
		this.productRules = productRules;
	}

	
}
