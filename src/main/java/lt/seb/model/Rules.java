package lt.seb.model;

public class Rules extends Product{

	private int minAge;
	private int maxAge;
	private int minIncome;
	private int maxIncome;
	private boolean studentRequirement;

	public Rules(int minAge, int maxAge, int minIncome, int maxIncome, boolean studentRequirement) {
		super();
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.minIncome = minIncome;
		this.maxIncome = maxIncome;
		this.studentRequirement = studentRequirement;
	}

	public Rules() {
		super();
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getMinIncome() {
		return minIncome;
	}

	public void setMinIncome(int minIncome) {
		this.minIncome = minIncome;
	}

	public int getMaxIncome() {
		return maxIncome;
	}

	public void setMaxIncome(int maxIncome) {
		this.maxIncome = maxIncome;
	}

	public boolean isStudentRequirement() {
		return studentRequirement;
	}

	public void setStudentRequirement(boolean studentRequirement) {
		this.studentRequirement = studentRequirement;
	}

}
