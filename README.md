﻿## About	

This is a single-page application that displays a list of products that are suitable for the customer by age and income.

## Running the application

**Requirements:**

- JRE 8 (Java Runtime Environment)
- Apache Tomcat 7 (developed and tested on)

**Running the application**

1. Run Apache Tomcat 7
2. Move the generated _seb.war_ file and the _admin_ folder from _seb/target_ directory to _tomcat/webapps_ folder
3. Access the application by typing _localhost:8080/admin_ in the address bar. 

